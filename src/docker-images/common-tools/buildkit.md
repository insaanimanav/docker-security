# Buildkit

With the overhaul in the docker build for 18.09 another enhancement was the integration of BuildKit.
Buildkit brings a huge chunk of features to docker build including better looking and better interpretable output. One  thing of importance to us is better **secrets management**

## To enable Buildkit

The easiest way is to

```shell
$ DOCKER_BUILDKIT=1 docker build .
```

To enable it by default set the daemon configuration in `/etc/docker/daemon.json`

```json
{ "features": { "buildkit": true } }
```

## Building with secrets

Now as stated in the pages before, it's not a very good idea to build with secrets embedded as ENV vars in the dockerfile , using buildkit is a better way.

You can basically use the --secret while using the docker build command to build the dockerfile with secrets
SO lets say you have a secret which needs to be added in the container.

### Step 1: Store the secret in a file

```shell
 echo 'WARMACHINEROX' > mysecret.txt
```

### Step 2: Create a Dockerfile and add the secrets using the RUN directive

```Dockerfile
# syntax=docker/dockerfile:1.2

FROM alpine

# shows secret from default secret location:
RUN --mount=type=secret,id=mysecret cat /run/secrets/mysecret

# shows secret from custom secret location:
RUN --mount=type=secret,id=mysecret,dst=/foobar cat /foobar
```

### Step 3: Pass the actual secrets using the docker build command

```shell
docker build --no-cache --progress=plain --secret id=mysecret,src=mysecret.txt .
```

