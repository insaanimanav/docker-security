# Use Multi Stage Builds

## The problem

* Sometimes, when building docker containers, you need to compile your application. Now if you compile things inside the container, you will end up with a lot of garbage and files your application doesn't need.
* More the files, more the attack surface. Also with this approach a lot of times your container ends up with a shell inside it, for example bash.
* As an attacker if I get access to your server I can easily get inside the docker container and change things without you knowing about it at all.
* Having files you don't need inside a container also leads to an increase in the size of the container which can lead to several issues such as increased deployment, push times , pull times etc.

## The solution

* The solution for all these problems highlighted above are to use multi-stage builds.
* Multi stage builds mean that, you compile your binary in one container which has all your tooling etc and then place and run that binary in another container which has just enough environment to run your binary.
* Docker provides a special container called scratch which has the just enough environment. You can't push, pull or tag the scratch container directly but you can use it in your Dockerfiles. Check the examples section to get an idea.
* Building from scratch also means that your container no longer has a shell I can get into directly.

## Example

Below is an example of a non-multi stage build -

```Dockerfile
FROM golang:1.14

RUN git clone https://github.com/coredns/coredns.git /coredns
RUN cd /coredns && make

EXPOSE 53 53/udp

CMD ["/coredns"]
```

The above leads to a size of 2.34GB and performs the same function 

Now in the case of multi-stage build

```Dockerfile
FROM golang:1.14

RUN git clone https://github.com/coredns/coredns.git /coredns
RUN cd /coredns && make

FROM scratch
COPY --from=0 /coredns/coredns /coredns

EXPOSE 53 53/udp
CMD ["/coredns"]
```

The above leads to a size of 51MB. 
