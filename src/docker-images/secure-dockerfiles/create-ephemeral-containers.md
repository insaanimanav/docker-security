# Create ephemeral containers

## The problem

* People often run stateful docker containers ie containers which are like snowflake systems in themselves.
* This is not a good practice since this leads to security issues.
* If you, yourself are making changes inside the container constanly , you have no way of know if a malicious actor made changes unless you go and check yourself.
* Most docker automation software assumes that your containers will be stateless and will delete and redeploy them  whenever needed.
* **Docker  containers are not VMs**

## The solution

* The Dockerfile which you write should generate as ephemeral containers as possible.
* The term ephermeral means that your containers using those images could be destroyed and re-created anytime.
* If you want to store anything that can or tends to change , prefer to use a volume mount.
* One important security advantage of having ephemeral containers is that if you somehow detect that your container has been compromised, an instant fix could be to instantly destroy and re-spawn the container.
* Instead of creating stateful containers and making changes inside them, use volume mounts so that those changes are persistent.

## Examples

```Dockerfile
COPY ./ /project_directory
```

This isn't a good idea, use a multi stage build if you want to run the binary or maybe use a volume mount using the -v flag to get these files inside the container.
