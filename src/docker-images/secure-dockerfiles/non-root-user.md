# Set non root user and group

## The problem

As you might already know, docker containers themselves run with root which poses a huge security problem.

> Would you run everything as root on your server ?

So why run root inside docker containers.

The problem with all this is that root inside containers can lead to a huge attack surface.

* The attacker could potentially mount any file system inside the container, read and change the files he normally wouldn't be allowed to.
* When you run non-root containers you limit what applications the container can run, you limit a huge chunk of the file system which can be written to inside the container.
* The idea to run root containers seems to be useful for development containers but can turn quickly turn to lethal when deployed in production.
* Some other things the root user can do is install packages , change config files bind privileged ports .

## The solution

* Don't run containers as root, use an un-priveleged user to run all your containers so that if anyone does get inside the container, they will possibly not be able to escalate priveleges and do major damage.
* Always keep in mind the [PoLP](https://en.wikipedia.org/wiki/Principle_of_least_privilege)


## Example

### Setup an unpriveleged user

```Dockerfile
#Pull the base image as Ubuntu
FROM ubuntu:latest

#Add a user with userid 8877 and name nonroot
RUN useradd −u 400 aastha

#Run Container as nonroot
USER aastha
.
.
.
.
.

```

The container generated using the above Dockerfile will not have root as the main user instead, the main user would be aastha. This would mean that while running the container and volume mounting anything that directory would also have to be owned aastha on your host system, so be wary of that.

### Completely disable the root user

With the above method you get containers which use the non-root user by default but still I could switch to the root user once inside the container. To prevent that you could run this inside the container using the RUN directive.

```shell
chsh /usr/sbin/nologin root
```

The above would ensure that even though I know then root password I cannot get into the container.
