# General Best practices

## Don't store secrets in environment variables

* This is one of the biggest docker security issues and you would be surprised at how much this comes up.
* I understand why that is, secret distribution is a hairy thing. You can either store secrets as environment variables or mount them in the filesystem at runtime.
* Dockerfiles are often distributed  with the code, so it is as good as embedding secrets in the code itself.
* Hence don't use `ENV` to store your secrets instead use volume mounts or maybe use [buildkit](https://docs.docker.com/develop/develop-images/build_enhancements/)

## Avoid using ADD

A small directive you can add to your dockerfiles is the ADD command.

So it expects you to do something like this

```Dockerfiles

ADD https://trustme.com/absolutely-trust-me.tar.gz
```

This comes with problems because you are randomly downloading a tarball without verifying it's integrity.

* I could hack trustme.com and change this tarball to something malicious and if you were lazy it would still get downloaded.
* So not a good idea.

* I would suggest using COPY here instead of add, download the tarball manually, verify it and then add it into an image.

**NOTE** : If your company uses some kind of fancy automated CI system,  then using ADD in your Dockerfiles is basically waiting for supply chain attack to happen.

## Avoid upgrading inside a container

* Don't run something like `apt update` inside the container itself.
* If you want the latest and greatest packages then fetch the latest image itself
* When you upgrade container packages you introduce more ambiguity, one of these could lead to vulnerable packages without you even knowing it and causing bigger problems.
* Also always remember  as a rule of thumb, Docker containers are ephemeral.

## Reduce the number of layers

* Try to have as little layers in your containers as possible.
* Each command like RUN, ADD, COPY will add a new layer
* So instead try to group commands
Doing

```Dockerfile
FROM ubuntu
RUN apt-get install -y wget
RUN wget https://…/downloadedfile.tar
RUN tar xvzf downloadedfile.tar
RUN rm downloadedfile.tar
RUN apt-get remove wget

```

is better than

```Dockerfile
FROM ubuntu
RUN apt-get install wget && wget https://…/downloadedfile.tar && tar xvzf downloadedfile.tar && rm downloadedfile.tar && apt-get remove wget
```
