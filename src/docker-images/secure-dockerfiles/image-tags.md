# Use proper tags

## The problem

> :latest is a proliferating ball of confusion that should be avaoided at all costs like a jar full of venomous snakes

* One important thing that I want to highlight is that latest isn't a dynamic tag . It's added by default by docker when a tag is not specified but it is a static tag.
* Pulling an image with the latest tag doesn't neccesarily mean that you will get the latest image. Anyone can explicit tag images with latest and stop that tagging scheme a little later, which means that you could be running outdated images.
* Using latest images also could lead to a nice supply chain attack, if I somehow find a way into the company's registry then I could push an image tagged latest and you would then be using my malicious image to run your infra.

## Solution

* Don't use :latest tagged images especially in automated build systems, it leads to a lot of ambiguity and apart from a broken system you could have a vulnerable system practically playing right into the hands of the attacker.

## Examples

`docker build . -t image:v1`
Use anything other than latest and let the consumers of the image also know that using latest isn't a good idea at all.