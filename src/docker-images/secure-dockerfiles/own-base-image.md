# Create your own base image

## The problem

* A lot of times we use prebuilt base images such as from ubuntu/ubuntu or debian/debian or whatever be your project, All these are good to use if we want to build one container and maybe run it on a server.
* But if we are into the business of building and serving docker containers on a daily basis, it is a good idea to build your own base image and use that.
* When we run someone else's base image even though they are trusted we have no way of knowing what all things are present inside the container itself.
* This can lead to both ambiguity and security vulnerabilities since we have no idea what packages are installed and how they are configured.

## The solution

* The solution to this problem is to create our own base image and use it as a golden image.
* Now when you create our own base image , you know exactly what things are installed and exactly how they are configured.
* The idea hence to use that to create and ship all your images, this would lead to huge reduction in the ambiguity and paranoia with respect to the base image being vulnerable.

## Example

[Here](https://github.com/moby/moby/tree/master/contrib) is a link to some examples which lead to the moby toolkit repo by docker.
