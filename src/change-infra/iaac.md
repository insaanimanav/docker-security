# Infrastructure as code

Infrastructure as code is another intereting aspect that has recently come up with the advent of the cloud and cloud based technologies.

The idea behind Infrastructure as code is that you can deploy all your services and your entire stack as plain-text. A sort of parser would then parse the file and deploy the infra.

## Mentioned below are some nice Infra as Code tools

### Ansible

* Ansible is one the most promising tools in the field of infrastructure as code.
* The idea with ansible is that you define a yaml file which is called a playbook.
* Ansible also lets you define roles which can be re-used very easily in playbooks.
[Ansible Documentation](https://docs.ansible.com/ansible/latest/user_guide/index.html)

### Puppet

Puppet uses its own declarative language which you can use to wirte configurations which are then deployed on your server 
[More about Puppet](https://puppet.com/learning-training/kits/puppet-language-basics/introduction/)

**Chef** is another intersting tool similar to the two above

## Why Infra as code ?

* If you can go all in on the Infrastructure as code thing then you can build systems that are not snowflake systems. snowflake systems mean that when you have a fleet of servers running the same service but each of them are configured manually , they are bound to be different from each other in some minute way or the other.
* Whereas if you deploy using tools like the above, each server is guaranteed to have the exact same configuration as the other one. Not only does this help in debugging it also helps in the security aspect since you don't have to manage the security stuff of multiple machines, you can do it once and let it redeploy each time.
* So the idea is to save all your playbooks/recipes/configurations in one machine or one server and you can use those to manage your entire fleet.
