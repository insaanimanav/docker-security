# Using immutable OS
 
The idea behind an immutable operating system is that it is mounted read-only ; also, conceptually, it is not patched but rather is replaced when it needs to be updated, in the same way as a container. This has obvious security advantages, as well as making features like rollback easier to implement.

## What, the files can't change ? Like at all ?

Not it can't , that's the whole idea. This stands by a principle called **security by knowledge** which basically means that if you have good knowledge of all the things that are running on your OS it makes securing it very easy.
You will have a very accurate idea of the attack surface in such a situation. 

## How do I install things ?

In immutable systems everything runs inside containers, which themselves are immutable by default. All the things that run are inside hence sanboxed and contained environments by default which leads to increase in the security.

## The security aspect

* You can have a very extensive knowledge of what is exactly running on your system..
* It makes it incredibly difficult to change anything even after the attacker has escalated priveleges.
* Immutability also makes incident response and forensics so much easier. You can compare precisely against an older image.
* Redeployement is also made very easy with immutability

## Some examples of immutable operating systems

* [Fedora Silverblue](https://docs.fedoraproject.org/en-US/fedora-silverblue/)
* [Fedora CoreOS](https://docs.fedoraproject.org/en-US/fedora-coreos/)
* [Opensuse MicroOS](https://microos.opensuse.org/)