# Summary
- [Securing Docker containers](securing-contaners.md)

- [Securing Docker Images](securing-images.md)
  - [General Best practices](docker-images/secure-dockerfiles/general-practices.md)
  - [Create ephemeral containers](docker-images/secure-dockerfiles/create-ephemeral-containers.md)
  - [Use Multi Stage Builds](docker-images/secure-dockerfiles/multi-stage-builds.md)
  - [Create your own base image](docker-images/secure-dockerfiles/own-base-image.md)
  - [Set non root user and group](docker-images/secure-dockerfiles/non-root-user.md)
  - [Use proper image tags](docker-images/secure-dockerfiles/image-tags.md)

- [Securing Docker runtime](securing-runtime.md)
  - [General Guidlines](securing-runtime/general-guidlines.md)
  - [AppArmor](securing-runtime/apparmor.md)
  - [SeLinux](securing-runtime/selinux.md)
  - [Seccomp](securing-runtime/seccomp.md)
  - [Capabilities](securing-runtime/capabilties.md)
  - [Memory restriction](securing-runtime/memory-management.md)

- [Changing your existing infra](change-infra.md)
  - [Infrastructure as code](change-infra/iaac.md)
  - [Using immutable OS](change-infra/immutable.md)

- [Common security tools](common-tools.md)
  - [Buildkit](common-tools/buildkit.md)
  - [hadolint](common-tools/hadolint.md)
  - [dockle](common-tools/dockle.md)
  - [OPA](common-tools/opa.md)
- [Referenc9es](references.md)