# Seccomp

Secure computing mode (seccomp) is a Linux kernel feature. You can use it to restrict the actions available within the container. The seccomp() system call operates on the seccomp state of the calling process. You can use this feature to restrict your application’s access.

Now if you want to check if your kernel supports seccomp 

```shell
grep CONFIG_SECCOMP= /boot/config-$(uname -r)
CONFIG_SECCOMP=y

```

> A large number of system calls are exposed to every userland process with many of them going unused for the entire lifetime of the process. A certain subset of userland applications benefit by having a reduced set of available system calls. The resulting set reduces the total kernel surface exposed to the application. System call filtering is meant for use with those applications. Seccomp filtering provides a means for a process to specify a filter for incoming system calls.

With seccomp you can basically define what syscalls your docker container has acccess to
With an empty seccomp profile like below in the file deny.json

```profile

{
     "defaultAction": "SCMP_ACT_ERRNO",
     "architectures": [
             "SCMP_ARCH_X86_64",
             "SCMP_ARCH_X86",
             "SCMP_ARCH_X32"
     ],
     "syscalls": [
     ]
}
```

Notice how there are no syscalls in the whitelist here.

```shell
docker container run --rm -it --cap-add ALL --security-opt apparmor=unconfined --security-opt seccomp=deny.json alpine sh
```

this gives the output

```shell

docker: Error response from daemon: exit status 1: "cannot start a container that has run and stopped\n".

```

This basically tells us that there is not enough permission to even start the container itself.

Now let's try [another profile](https://raw.githubusercontent.com/docker/labs/master/security/seccomp/seccomp-profiles/default-no-chmod.json) from the docker container security labs.

* Now if you load this profile you will not be able to use the chmod command
* That is because the chmod syscall is removed from the whitelist

## Conclusion

Overall seccomp can help you accurately define the exact  syscalls and hence defining the exact set of permission the container needs. Which greatly greatly improves your security profile.
