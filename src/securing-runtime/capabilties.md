# Capabilities

## What are capabilties

Basically the idea behind capabilties is that it lets us break the entire power of root into smaller portions so we can distribute it without giving away the entire root permissions.

Currently docker containers by default get the following capabilities.

```c
CHOWN, DAC_OVERRIDE, FSETID, FOWNER, MKNOD, NET_RAW, SETGID, SETUID, SETFCAP, SETPCAP, NET_BIND_SERVICE, SYS_CHROOT, KILL, AUDIT_WRITE

```

Now let's take an example, lets say we are running a container that runs ntpd which modifies system time so hence it would need a capbility like `CAP_SYS_TIME` so we would modify the capabilties to add that. A dumb thing to do would be to run the container in priveleged mode which would give the container most of the other capabilties it doesn't need.

Now to run an ntpd container we can just run,

```shell

docker run -d --cap-add SYS_TIME ntpd

```

Another way to try this would be to maybe drop all the capabilties from a container and then add those one by one . 

To drop all capabilties type you can use the `--cap-drop ALL` option.


```shell

docker run --cap-drop ALL --cap-add SYS_TIME ntpd /bin/sh

```

This would drop all capabilities and just assign whatever is needed.
