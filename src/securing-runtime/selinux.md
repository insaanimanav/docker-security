# SeLinux

SELinux is an open source project which was released in 2000 and integrated into the Linux kernel in 2003 .
According to Redhat's explainer

> SELinux is a security architecture for Linux systems that allows administrators to have more control over who can access the system.

Now the first step is to make sure that SELinux is enabled:

```shell
sestatus

SELinux status:                 enabled
SELinuxfs mount:                /sys/fs/selinux
SELinux root directory:         /etc/selinux
Loaded policy name:             targeted
Current mode:                   enforcing
Mode from config file:          enforcing
Policy MLS status:              enabled
Policy deny_unknown status:     allowed
Memory protection checking:     actual (secure)
Max kernel policy version:      33

```

In traditional Unix based systems we used to have DAC which stands for Direct Access Control but SELinux introduces something called MAC or Mandatory Access control.


## How does SELinux work ?

* The idea is that everything in SELinux is a Label. So Files, processes ports etc all have an SELinux label attached to them 
* The kernel manages all them in memory 
* Format -

```shell
user:role:type:level

```

So SELinux uses type enforcement, this is useful when for example an httpd_t context label is able to interact with an httpd_log context label but its not allowed to interact with shadow_t label

So type enforcement is the part of the policy that says for instance "a process running with the label httpd_t can have read access to a file labelled httpd_config_t

## SELinux for containers

SELinux policies for containers are defined by the container-selinux package. Docker CE requires this package (along with its dependencies) so that the processes and files created by Docker are able to run with limited system access. Containers leverage the container_t label which is simply an alias to svirt_lxc_net_t and container_file_t which is an alias to svirt_sandbox_file_t.

By default, containers are run with the label container_t and are allowed to read/execute under /usr and read most content from /etc. The files under /var/lib/docker and /var/lib/containers have the label container_var_lib_t.

Files labeled with container_file_t are the only files that are writable by containers. If you want a volume mount to be writeable, you will needed to specify :z or :Z at the end. Their behavior is different so be careful which one you are using.

## SELinux labels

SELinux itself runs on types, We use types to protect the host from the container. But we could also adjust the types to control what controls what network ports are allowed into and out of the container. Currently, we run all containers with the svirt_net_lxc_t. This type is allowed to listen on all network ports and allowed to connect out on all network ports. We could tighten the security on the container by adjusting the SELinux type label.

With regular SELinux and Apache httpd, we by default only allow the apache process to listen on the Apache ports (http_port_t)

```shell
sudo sepolicy network -t http_port_t

http_port_t: tcp: 80,81,443,488,8008,8009,8443,9000
```

We also block all outgoing port connections. This helps us lock down the Apache process, and even if a hacker were to subvert an application with a security vulnerability like ShellShock, we could stop the application from becoming a spam bot, or allowing the process to initiate attacks on other systems. It is like Hotel California, "You can check in any time you want, but you can never leave."

With containers, however, if you were running an Apache server application within a container, and the application were subverted, the Apache process would be able to connect to any network ports and become a spam bot, or attack other hosts/containers via the network.

It is fairly simple to create a new policy type to run with your containers using SELinux. First, you could create an SELinux TE (Type Enforcement) file.

```shell

# cat > docker_apache.te << _EOF

policy_module(docker_apache,1.0)

# This template interface creates the docker_apache_t type as a
# type which can be run as a docker container. The template
# gives the domain the least privileges required to run.
virt_sandbox_domain_template(docker_apache)

# I know that the apache daemon within the container will require
# some capabilities to run. Luckily I already have policy for
# Apache and I can query SELinux for the capabilities.
# sesearch -AC -s httpd_t -c capability
allow docker_apache_t self: capability { chown dac_override kill setgid setuid net_bind_service sys_chroot sys_nice sys_tty_config } ;

# These are the rules required to allow the container to listen
# to Apache ports on the network.

allow docker_apache_t self:tcp_socket create_stream_socket_perms;
allow docker_apache_t self:udp_socket create_socket_perms;
corenet_tcp_bind_all_nodes(docker_apache_t)
corenet_tcp_bind_http_port(docker_apache_t)
corenet_udp_bind_all_nodes(docker_apache_t)
corenet_udp_bind_http_port(docker_apache_t)

# Apache needs to resolve names against a DNS server
sysnet_dns_name_resolve(docker_apache_t)

# Permissive domains allow processes to not be blocked by SELinux
# While developing and testing your policy you probably want to
# run the container in permissive mode.
# You want to remove this rule, when you are confident in the
# policy.
permissive docker_apache_t;
_EOF

# make -f /usr/share/selinux/devel/Makefile docker_apache.pp
# semodule -i docker_apache.pp

```

Now run the container with the new type:

```shell

# docker run -d --security-opt type:docker_apache_t httpd

```

Now this container would run with much tighter SELinux security then a normal container. Note you probably would need to watch the audit logs to see if your app needs additional SELinux allow rules.

You could add these rules by using the audit2allow command and appending the rules onto the existing .te file, recompile and install.

```shell

# grep docker_apache_t /var/log/audit/audit.log | audit2allow >> docker_apache.te
# make -f /usr/share/selinux/devel/Makefile docker_apache.pp
# semodule -i docker_apache.pp

```
