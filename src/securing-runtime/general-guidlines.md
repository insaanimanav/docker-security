# General Guidlines

## Do not expose the docker daemon socket

Do not enable tcp on the Docker daemon socket. If you are running docker daemon with -H tcp://0.0.0.0:XXX or similar you are exposing un-encrypted and un-authenticated direct access to the Docker daemon. If you really, really have to do this, you should secure it. Check how to do this following Docker official documentation.

Do not expose /var/run/docker.sock to other containers. If you are running your docker image with -v /var/run/docker.sock://var/run/docker.sock or similar, you should change it. Remember that mounting the socket read-only is not a solution but only makes it harder to exploit. Equivalent in the docker-compose file is something like this:

```yml

volumes:
- "/var/run/docker.sock:/var/run/docker.sock"

```

## Avoid Depending on Security Anti-Patterns

Docker-based applications should not depend on security anti-patterns to function correctly. Docker containers should not require things like host root access, elevating privilege, or disabling protections like SELinux. If planned software features require any of these, it might be good to talk through the design with your development teams to see if there are alternatives.

## Mount Container’s Root Filesystem as Read-Only

Containers should be run with their root filesystems in read-only mode. This isolate writes to specifically defined directories, which can then be easily monitored. Additionally, using read-only filesystems makes containers more resilient to being compromised. Data should also not be written within containers. This is just a standard piece following the best practice of immutable infrastructure and reduces 
attack vectors since the instance cannot be written to. There should be an explicitly defined volume for writing for the container.

## Unbounded Network Access from Containers

Organizations should control the egress network traffic sent by containers. Tools for monitoring the inter-container traffic should at the very least accomplish the following:

* Automated determination of proper container networking surfaces, including inbound and process-port bindings.
* Detection of traffic flow both between containers and other network entities.
* Detection of network anomalies, such as unexpected traffic flows within the organization’s network and port scanning.

## Do Not Expose Unused Ports

The Dockerfile defines which ports will be opened by default on a running container. Only the ports that are needed and relevant to the application should be open. If there is access to the Dockerfile this can be evident by looking for the EXPOSEinstruction.

## Don't bind  with 0.0.0.0

Another great practice is don't bind with 0.0.0.0 in case of ports when not needed. binding with 0.0.0.0 means that you can access that outside your docker container network which isn't really desirable practice if not needed 100 percent.

## Don't run ssh inside your containers 

As obvious as it seems I have still seen people do that. So the idea is not to run any sort of ssh server or remote access thing inside a docker container. You can use the docker exec command to execute commands in the container like shown below 

```shell

docker exec -it <container id> /bin/bash

```

