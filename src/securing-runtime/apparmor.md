# AppArmor

## What is AppArmor

AppArmor is a Linux security module that protects an operating system and its application from security threats.

Docker automatically generates and loads a default profile for containers named docker-default. The Docker binary generates this  profile in `tmps` and then it loads it into the kernel.

When you run a container it uses the docker-default policy which is moderately protective in nature but a better way would be to write a custom policy for specific containers.

Now let's say you have a custom profile you want to load, you can use this below command to do that.

```shell
apparmor_parser -r -W /path/to/your_profile
```

Now you can run your custom profile with --security-opt like so 

```shell
docker run --rm -it --security-opt apparmor=your_profile hello-world
```

To unload your profiles

```shell
apparmor_parser -R /path/to/profile
```

Here are some more resources for writing profiles

[Quick Profile Language](https://gitlab.com/apparmor/apparmor/-/wikis/QuickProfileLanguage)
[Policy Reference](https://gitlab.com/apparmor/apparmor/-/wikis/AppArmor_Core_Policy_Reference#AppArmor_globbing_syntax)

## Example profile

Below is an example profile for an nginx container

```profile
#include <tunables/global>


profile docker-nginx flags=(attach_disconnected,mediate_deleted) {
  #include <abstractions/base>

  network inet tcp,
  network inet udp,
  network inet icmp,

  deny network raw,

  deny network packet,

  file,
  umount,

  deny /bin/** wl,
  deny /boot/** wl,
  deny /dev/** wl,
  deny /etc/** wl,
  deny /home/** wl,
  deny /lib/** wl,
  deny /lib64/** wl,
  deny /media/** wl,
  deny /mnt/** wl,
  deny /opt/** wl,
  deny /proc/** wl,
  deny /root/** wl,
  deny /sbin/** wl,
  deny /srv/** wl,
  deny /tmp/** wl,
  deny /sys/** wl,
  deny /usr/** wl,

  audit /** w,

  /var/run/nginx.pid w,

  /usr/sbin/nginx ix,

  deny /bin/dash mrwklx,
  deny /bin/sh mrwklx,
  deny /usr/bin/top mrwklx,


  capability chown,
  capability dac_override,
  capability setuid,
  capability setgid,
  capability net_bind_service,

  deny @{PROC}/* w,   # deny write for all files directly in /proc (not in a subdir)
  # deny write to files not in /proc/<number>/** or /proc/sys/**
  deny @{PROC}/{[^1-9],[^1-9][^0-9],[^1-9s][^0-9y][^0-9s],[^1-9][^0-9][^0-9][^0-9]*}/** w,
  deny @{PROC}/sys/[^k]** w,  # deny /proc/sys except /proc/sys/k* (effectively /proc/sys/kernel)
  deny @{PROC}/sys/kernel/{?,??,[^s][^h][^m]**} w,  # deny everything except shm* in /proc/sys/kernel/
  deny @{PROC}/sysrq-trigger rwklx,
  deny @{PROC}/mem rwklx,
  deny @{PROC}/kmem rwklx,
  deny @{PROC}/kcore rwklx,

  deny mount,

  deny /sys/[^f]*/** wklx,
  deny /sys/f[^s]*/** wklx,
  deny /sys/fs/[^c]*/** wklx,
  deny /sys/fs/c[^g]*/** wklx,
  deny /sys/fs/cg[^r]*/** wklx,
  deny /sys/firmware/** rwklx,
  deny /sys/kernel/security/** rwklx,
}
```

* The above policy as you can see is very specific in what it allows through and what it doesn't which means that your nginx container will run with very restricted priveleges.

Now let us load this profile

```shell
sudo apparmor_parser -r -W /etc/apparmor.d/containers/docker-nginx
```

Now let's run the nginx container itself in detached mode.

```shell

docker run --security-opt "apparmor=docker-nginx" \
     -p 80:80 -d --name apparmor-nginx nginx

```

Exec into the container

```shell

docker container exec -it apparmor-nginx bash

```

Now Let's try some basic operations to test out this profile

```shell
root@6da5a2a930b9:~# ping 8.8.8.8
ping: Lacking privilege for raw socket.

root@6da5a2a930b9:/# top
bash: /usr/bin/top: Permission denied

root@6da5a2a930b9:~# touch ~/thing
touch: cannot touch 'thing': Permission denied

root@6da5a2a930b9:/# sh
bash: /bin/sh: Permission denied

root@6da5a2a930b9:/# dash
bash: /bin/dash: Permission denied

```

So in the above we can clearly see how it restricts the nginx container from doing things and accessing files it doesn't have to or doesn't need to.
Hence if you can accurately know what locations and things your container needs access to then the best idea is to setup an apparmor profile like the above and use this diligently. 
Using apparmor means that you don't have to take care of any special flag while launching a container to ensure security and other similar things.

Reference : [Docker AppArmor reference](https://docs.docker.com/engine/security/apparmor/)
